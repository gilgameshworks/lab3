import pandas as pd
import pyarrow as pa
import pyarrow.parquet as pq
import pymysql
from pyspark.sql import SparkSession
from pyspark.ml.clustering import KMeans
from pyspark.ml.feature import VectorAssembler, StandardScaler
import numpy as np


def Predictor(self):

    def __init__(self):
        with open('api_key.txt', 'r') as f:
            self.log, self.pas = str(f.readline()).split()
        self.cnx = pymysql.connect(user=self.log, password=self.pas, host='localhost')
        cursor = self.cnx.cursor()
        self.spark = SparkSession.builder.appName("MyApp").getOrCreate()


    def read_from_database(self):
        # Чтение данных из базы данных
        df = pd.read_sql("SELECT * FROM lab3.food_clusters", self.cnx)
        df.to_parquet('df.parquet')
        self.cxn.close()

    def prepare_data(self):
        # Преобразование данных
        parquet_df = self.spark.read.parquet('food.parquet')
        assembler = VectorAssembler(inputCols=['energy_100g', 'proteins_100g', 'fat_100g',
            'carbohydrates_100g', 'sugars_100g', 'energy-kcal_100g',
            'saturated-fat_100g', 'salt_100g', 'sodium_100g', 'fiber_100g',
            'fruits-vegetables-nuts-estimate-from-ingredients_100g',
            'nutrition-score-fr_100g'], outputCol="features")
        df = assembler.transform(parquet_df)

        # Scale the features column
        scaler = StandardScaler(inputCol="features", outputCol="scaled_features", withStd=True, withMean=False)
        scaler_model = scaler.fit(df)
        self.df = scaler_model.transform(df)

    def model_train(self):
        # Обучение модели
        kmeans = KMeans(k=10, seed=123)
        self.model = kmeans.fit(self.df)

    def predict(self):
        # Предсказание
        predictions = self.model.transform(self.df)

        predictions_df = predictions.select("prediction", "scaled_features")

        # Лист предсказаний
        self.preds = predictions_df.collect()

        # Значения
        for row in self.preds[0:5]:
            print(row)

    def write_to_database(self):
        # Запись в базу данных
        cnx = pymysql.connect(user=self.log, password=self.pas, host='localhost')
        cursor = cnx.cursor()


        for row in self.preds[0:5]:
            z = np.append(row[1],row[0])
            z = z.tolist()
            print(len(z))
            cursor.execute("INSERT INTO lab3.food_clusters VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)", z)
            print(z)
            
        cnx.commit()   
        self.cnx.close()     


    def closer(self):
        self.cnx.close()
        self.spark.stop()

if __name__ == '__main__':
    predictor = Predictor()
    predictor.read_from_database()
    predictor.prepare_data()
    predictor.model_train()
    predictor.predict()
    predictor.write_to_database()