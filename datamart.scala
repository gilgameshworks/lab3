// spark-shell

object DFFromCSV{
    def main(args: Array[String]): Unit = {
        val df = spark.read.options(Map("delimiter" -> "\t", "header" -> "true")).csv("en.openfoodfacts.org.products.csv")

        val Col = Seq('energy_100g', 'proteins_100g', 'fat_100g',
            'carbohydrates_100g', 'sugars_100g', 'energy-kcal_100g',
            'saturated-fat_100g', 'salt_100g', 'sodium_100g', 'fiber_100g',
            'fruits-vegetables-nuts-estimate-from-ingredients_100g',
            'nutrition-score-fr_100g')
        val data = df.select(Col.head, Col.tail: _*)
        data.write.options(Map( "header" -> "true")).csv("food_datamart.csv")
    }
}

DFFromCSV.main(Array("Datamart"))
